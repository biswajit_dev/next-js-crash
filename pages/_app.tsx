import "../styles/global.scss";

const MainApp = ({ Component, prevProps }) => {
    return <Component {...prevProps} />
}

export default MainApp;